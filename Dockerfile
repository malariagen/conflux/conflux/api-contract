FROM python:3.8.3-alpine3.12

ARG PORT=5000
ARG CONTRACT=api.yaml

ENV SSO_HOST cas-dev.malariagen.net

EXPOSE $PORT

WORKDIR /app

RUN apk add --no-cache tini

COPY requirements.txt /tmp
RUN pip3 install --no-cache-dir -r /tmp/requirements.txt

COPY ./$CONTRACT /app/contract.yaml
COPY ./app.py /app/app.py

RUN echo "#!/bin/sh" > /app/start.sh \
&& echo "cp /app/contract.yaml /app/contract-run.yaml" >> /app/start.sh \
&& echo "sed -i 's/{{ sso }}/$SSO_HOST/' /app/contract.yaml" >> /app/start.sh \
&& echo "python app.py" >> /app/start.sh

RUN chmod u+x /app/start.sh

ENTRYPOINT ["/sbin/tini", "--"]
CMD ["/app/start.sh"]
