# api-contract

OpenAPI contract for api-service

## Running mocked service locally

You can either run the api-contract as a mocked service by using the provided `docker` image or by using the python module `connexion`

### Docker

The docker image defaults to port `5000` but a `--build-arg` of `PORT=[port]` can be used to change the port number

1. Build the docker image. `docker build . --tag=mock-api`
2. Run `docker run -it --rm -p 5000:5000 mock-api`

### Connexion

Install the following
- python >=3.5
- connexion==2.7.0
- connexion[swagger-ui]
- swagger-ui-bundle==0.0.6
- Flask==1.1.2

Run using the following

1. `connexion run api.yaml --mock=all --port=5000`

### Testing

The mocked contract will return values found in the example sections of the components schema. You can view these examples as well as all possible endpoints via the swagger ui by going to `http://localhost:5000/ui`
