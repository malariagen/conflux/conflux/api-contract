import connexion
from flask_cors import CORS
from connexion.mock import MockResolver

resolver = MockResolver(mock_all='all')

app = connexion.FlaskApp(__name__)
app.add_api('/app/contract.yaml', resolver=resolver)

# add CORS support
CORS(app.app)

if __name__ == "__main__":
  app.run(port=5000)
